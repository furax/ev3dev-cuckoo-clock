#!/usr/bin/env python3
from ev3dev.ev3 import *
from datetime import datetime
from threading import Timer
from time import sleep
import logging


class Hand():
    SPEED_NORMAL  = 30  # Speed during normal operation
    SPEED_SEARCH  = 180 # Speed during calibration search phase (when we're looking for next touch to measure full loop duration)
    SPEED_PRECISE = 30  # Speed during calibration precise phase (looking for touch event bounds)
    
    CALIBRATION_MARGIN  =   70 # Tolerance for touch re-triggering in motor position units
    CALIBRATION_LOOPS   =    5 # Number of loops used to fully calibrate hand
    
    #Phases of calibration process
    CALIBRATION_PHASE_NOT_CALIBRATED            =  0 # Calibration was never done, can't operate normally as we don't know hand's geometry
    CALIBRATION_PHASE_SEARCH                    =  1 # Run through the loop looking for the touch event
    CALIBRATION_PHASE_SEARCH_TOUCHED            =  2 # Touch event triggered. Now go forward until we're out of touch zone
    CALIBRATION_PHASE_SEARCH_UNTOUCHED          =  3 # Touch zone passed. Let's go a bit further forward to make sure sensor won't trigger again and then measure its bounds
    CALIBRATION_PHASE_PRECISE_BACK              =  4 # Go back until touch event is triggered
    CALIBRATION_PHASE_PRECISE_BACK_TOUCHED      =  5 # Go back to determine touch end bound
    CALIBRATION_PHASE_PRECISE_BACK_UNTOUCHED    =  6 # Go back a bit more to make sure sensor won't re-trigger
    CALIBRATION_PHASE_PRECISE_FORWARD           =  7 # Go forward until touch event is triggered
    CALIBRATION_PHASE_PRECISE_FORWARD_TOUCHED   =  8 # Go forward to determind touch end bound
    CALIBRATION_PHASE_PRECISE_FORWARD_UNTOUCHED =  9 # Go forward a bit more to make sure sensor won't re-trigger
    CALIBRATION_PHASE_DONE                      = 10 # Calibration is over for this hand
    
    ERROR_RANGE = 0.1 # Max error to not get discarded (fraction of full loop)
    
    def __init__(self, motor, sensor, function, interval, polarity = 'normal'):
        self.motor = LargeMotor(motor)
        self.sensor = TouchSensor(sensor)
        self.function = function
        self.interval = interval
        self.polarity = polarity
        
        self.motor.reset()
        self.motor.polarity = polarity
        self.motor.stop_action = 'hold'
        
        self.calibration = self.CALIBRATION_PHASE_NOT_CALIBRATED
        
    def BeginCalibration(self):
        self.calibration = self.CALIBRATION_PHASE_SEARCH
        self.zero = []
        self.touch_to_zero = 0
        self.motor.stop_action = 'hold'
        self.target = self.motor.position
    
    def Calibrate(self):
        if self.calibration == self.CALIBRATION_PHASE_DONE:
            return False # All values acquired, nothing more to do
        elif self.calibration == self.CALIBRATION_PHASE_SEARCH:
            self.motor.run_forever(speed_sp = self.SPEED_SEARCH) # Going fast until we're near top position
            if self.sensor.value() == 1: # We're somewhere near top position. Let's go past touch area and then find its bounds with lower speed
                self.calibration = self.CALIBRATION_PHASE_SEARCH_TOUCHED
        elif self.calibration == self.CALIBRATION_PHASE_SEARCH_TOUCHED:
            if self.sensor.value() == 0: # OK, looks like we're past touch area
                self.calibration = self.CALIBRATION_PHASE_SEARCH_UNTOUCHED
                self.calibration_bound = self.motor.position + self.CALIBRATION_MARGIN # Margin guarantees touch sensor won't retrigger
            self.StepForward() # Slow down
        elif self.calibration == self.CALIBRATION_PHASE_SEARCH_UNTOUCHED:
            if self.sensor.value() == 1: # Touch sensor re-triggered. OK, let's pass this zone, too, and define new right margin
                self.calibration = self.CALIBRATION_PHASE_SEARCH_TOUCHED
            elif self.motor.position >= self.calibration_bound: # We're past touch zone, now let's go back and find out where the sensor triggers this way
                self.calibration = self.CALIBRATION_PHASE_PRECISE_BACK
                self.StepBack()
                return True
            self.StepForward()
        elif self.calibration == self.CALIBRATION_PHASE_PRECISE_BACK:
            if self.sensor.value() == 1: # We entered touch zone
                self.onBack = self.motor.position # Will be updated only once
                self.calibration = self.CALIBRATION_PHASE_PRECISE_BACK_TOUCHED
            self.StepBack()
        elif self.calibration == self.CALIBRATION_PHASE_PRECISE_BACK_TOUCHED:
            if self.sensor.value() == 0: # Touch zone end
                self.offBack = self.motor.position # Can be updated more than once
                self.calibration_bound = self.offBack - self.CALIBRATION_MARGIN # Margin to look for re-triggering
                self.calibration = self.CALIBRATION_PHASE_PRECISE_BACK_UNTOUCHED
            self.StepBack()
        elif self.calibration == self.CALIBRATION_PHASE_PRECISE_BACK_UNTOUCHED:
            if self.sensor.value() == 1: # Sensor re-triggered
                self.calibration = self.CALIBRATION_PHASE_PRECISE_BACK_TOUCHED # Touch area continues. We do not update onBack as we're interested in FIRST bound. offBack will be updated, though.
            elif self.motor.position <= self.calibration_bound: # We're past touch zone
                self.calibration = self.CALIBRATION_PHASE_PRECISE_FORWARD
                self.StepForward()
                return True
            self.StepBack()
        elif self.calibration == self.CALIBRATION_PHASE_PRECISE_FORWARD:
            if self.sensor.value() == 1: # We've entered touch zone
                self.onForward = self.motor.position # Will be updated only once
                self.calibration = self.CALIBRATION_PHASE_PRECISE_FORWARD_TOUCHED
            self.StepForward()
        elif self.calibration == self.CALIBRATION_PHASE_PRECISE_FORWARD_TOUCHED:
            if self.sensor.value() == 0: # We've exited touch zone
                self.offForward = self.motor.position # Can be updated more than once
                self.calibration_bound = self.offForward + self.CALIBRATION_MARGIN # Margin to look for re-triggering
                self.calibration = self.CALIBRATION_PHASE_PRECISE_FORWARD_UNTOUCHED
            self.StepForward()
        elif self.calibration == self.CALIBRATION_PHASE_PRECISE_FORWARD_UNTOUCHED:
            if self.sensor.value() == 1: # Sensor re-triggered
                self.calibration = self.CALIBRATION_PHASE_PRECISE_FORWARD_TOUCHED # Touch area continues. We do not update onForward as we're interested in FIRST bound. offForward will be updated, though.
            elif self.motor.position >= self.calibration_bound: # We're past touch zone
                zero = (self.onBack + self.offBack + self.onForward + self.offForward) / 4 # No better way to determine top position, sorry
                logging.debug('Hand ' + self.motor.address + ': ' + str(self.offBack) + ' << ' + str(self.onBack) + ', ' + str(self.onForward) + ' >> ' + str(self.offForward) + '; 0 at ' + str(zero))
                self.zero.append(zero)
                self.touch_to_zero = self.touch_to_zero + zero - self.onForward # Sum now, average later
                if len(self.zero) >= self.CALIBRATION_LOOPS:
                    self.calibration = self.CALIBRATION_PHASE_DONE
                    self.motor.stop()
                    return False
                else:
                    self.calibration = self.CALIBRATION_PHASE_SEARCH # Let's do one more loop
                    return True
            self.StepForward()
        return True
    
    def FinishCalibration(self):
        # Difference between top position and touch even position when moving hand forward
        self.touch_to_zero = self.touch_to_zero / len(self.zero)
        assert self.touch_to_zero > 0
        
        # Current values
        self.last_zero = self.zero[len(self.zero)-1]
        self.last_value = self.function()

        # These values won't change
        self.loop = (self.last_zero - self.zero[0]) / (len(self.zero) - 1)
        self.motor.stop_action = 'hold'
        
        logging.info('Hand ' + self.motor.address + ':')
        logging.info('One loop is ' + str(self.loop) + ' motor steps')
        logging.info('Touch event preceeds top position in ' + str(self.touch_to_zero) + ' motor steps')
        logging.info('Current top position is at ' + str(self.last_zero) + ' motor steps')
        
        # We don't recalibrate this loop as we can have e. g. 11:59 when we'll have a big chance to enter touch zone on high speed. Let's delay until we speed down
        self.next_calibration = self.last_zero + self.loop * 1.5
        
    def RunTo(self, position, speed):
        self.target = position
        self.motor.run_to_abs_pos(position_sp = position, speed_sp = speed)
        
    def StepBack(self):
        if self.motor.position <= self.target:
            self.RunTo(self.motor.position - 1, self.SPEED_PRECISE)
        else:
            self.RunTo(self.target, self.SPEED_PRECISE)
            
    def StepForward(self):
        if self.motor.position >= self.target:
            self.RunTo(self.motor.position + 1, self.SPEED_PRECISE)
        else:
            self.RunTo(self.target, self.SPEED_PRECISE)
        
    def Move(self):
        assert self.calibration == self.CALIBRATION_PHASE_DONE
        
        value = self.function()
        if value < self.last_value: # Passing through top position. We don't want hand move backward one loop
            self.last_zero = self.last_zero + self.loop
            
            # Let's reset the motor as well to drop accumulated error caused by big position values
            self.last_zero = self.last_zero - self.motor.position
            self.next_calibration = self.next_calibration - self.motor.position
            self.motor.reset()
            self.motor.polarity = self.polarity
            self.motor.stop_action = 'hold'
        
        if self.motor.position > self.next_calibration and self.sensor.value() == 1: # We entered touch zone. Let's see how much of error we have
            error = self.motor.position - (self.last_zero + self.loop - self.touch_to_zero)
            while error > self.loop/2:
                error = error - self.loop
            while error < -self.loop/2:
                error = error + self.loop
            if error != 0:
                if error > self.loop * self.ERROR_RANGE or error < -self.loop * self.ERROR_RANGE:
                    # Sometimes the button may bounce unexpectedly (or even remain touched after the arrow leaves top position). Let's avoid these hardware issues causing hand mispositioning
                    logging.warning('Hand ' + self.motor.address + ': Error is too big: ' + str(error) + ', discarding')
                else:
                    logging.info('Hand ' + self.motor.address + ': Error between expected and actual touch position: ' + str(error) + '. Correcting zero value. Old zero value was ' + str(self.last_zero) + ', new value will be ' + str(self.last_zero + error))
                    self.last_zero = self.last_zero + error # The easiest variable to correct
            self.next_calibration = self.last_zero + self.loop * 1.5 # Until next time
        
        self.RunTo(self.last_zero + self.loop * value, self.SPEED_NORMAL)
        self.last_value = value
        
        self.timer = Timer(self.interval, self.Move)
        self.timer.start()
        
        
def HourFunction():
    dt = datetime.now()
    return (dt.hour % 12 + dt.minute / 60) / 12

def MinuteFunction():
    dt = datetime.now()
    return (dt.minute + dt.second / 60) / 60

def SecondFunction():
    dt = datetime.now()
    return (dt.second + dt.microsecond / 1000000) / 60

def CalibrateHands(hands):
    for hand in hands:
        hand.BeginCalibration()
    while True:
        calibrating = False
        for hand in hands:
            calibrating = hand.Calibrate() or calibrating
        if not calibrating: # Wait until at least one hand is calibrating
            break
        sleep(0.01)
    for hand in hands:
        hand.FinishCalibration()
        
        
class LedIndication():
    def __init__(self):
        # We're in startup phase, let's go red
        Leds.set_color(Leds.LEFT, Leds.RED)
        Leds.set_color(Leds.RIGHT, Leds.RED)
        
        self.phases = [Leds.BLACK, Leds.GREEN, Leds.GREEN, Leds.BLACK]
        self.phase = 0
    
    def Run(self):
        # Not using regular 'timer' trigger here, so if the program terminates we can see that LEDs stopped blinking
        Leds.set_color(Leds.RIGHT, self.phases[self.phase])
        self.phase = (self.phase + 1) % len(self.phases)
        Leds.set_color(Leds.LEFT, self.phases[self.phase])
        
        self.timer = Timer(2, self.Run)
        self.timer.start()


class Pendulum():
    def __init__(self, motor, forwardEndSensor, backwardEndSensor, speed):
        self.motor = LargeMotor(motor)
        self.motor.reset()
        self.endSensor = [
            TouchSensor(forwardEndSensor),
            TouchSensor(backwardEndSensor)
        ]
        self.direction = 0
        self.speed = speed
        self.steps = [2*speed, -2*speed]
        
    def Run(self):
        if self.endSensor[self.direction].value() == 1: # Weight is on the top. It'll break throgh the mechanics if we continue. Reversing the direction...
            self.direction = 1 - self.direction
            self.motor.reset() # To avoid position drift
        if datetime.now().hour >= 10: # Pendulum mechanics is noisy during nights, so silence then.
            self.motor.run_to_rel_pos(position_sp = self.steps[self.direction], speed_sp = self.speed)
        
        self.timer = Timer(1, self.Run)
        self.timer.start()
    
class Cuckoo():
    def __init__(self, motor, speed, duration, sound_timeout, single, half):
        self.motor = MediumMotor(motor)
        self.motor.reset()
        
        self.speed = speed
        self.duration = duration
        self.shift = self.speed * self.duration # Position shift to fully roll out the bird
        self.sound_timeout = sound_timeout
        self.single = single
        self.half = half
        
        self.last = datetime.now()
        self.PullBack(True)
        
    def Run(self):
        now = datetime.now()
        last = self.last
        self.last = now # To make sure we sound only once each half-hour
        
        if self.Enabled(now): # Counting hours
            if last.minute == 59 and now.minute == 0:
                self.count = 12 - (24 - now.hour) % 12
                self.motor.reset() # To avoid position drift
                self.Act()
                return
            elif last.minute == 29 and now.minute == 30: # Short half-hour sound
                Sound.play(self.half).wait()
    
        self.timer = Timer(1, self.Run)
        self.timer.start()
        
    def PullBack(self, done = False):
        self.motor.run_to_rel_pos(position_sp = -self.shift, speed_sp = self.speed)
        if not done: # We're in the middle of acting
            self.timer = Timer(self.duration, self.Act)
            self.timer.start()
        
    def Act(self):
        if self.count == 0:
            self.Run()
            return
            
        self.count = self.count - 1
        self.motor.run_to_rel_pos(position_sp = self.shift, speed_sp = self.speed)
        self.last = datetime.now()
        
        self.timer = Timer(self.sound_timeout, self.Cuckoo) # We want our bird to sound when it's moved out, so delaying the sound a little
        self.timer.start()
        
    def Cuckoo(self):
        Sound.play(self.single).wait()
        passed = datetime.now() - self.last
        if passed.total_seconds() >= self.duration: # It's already time to pull back
            self.PullBack()
        else: # The bird is yet moving forward; delaying pullback
            self.timer = Timer(self.duration - passed.total_seconds(), self.PullBack)
            self.timer.start()
        
    def Enabled(self, now):
        if now.hour == 0 and now.minute > 1 or now.hour > 0 and now.hour < 10: # Nights
            return False
        if now.weekday() == 4 and (now.hour == 12 or now.hour == 13 and now.minute < 30): # I have weekly calls on Friday so don't want to be disturbed
            return False
        return True


logging.basicConfig(filename='cuckoo-clock.log', level=logging.DEBUG)
        

indication = LedIndication()
pendulum = Pendulum('outC', 'in3', 'in4', 30)
cuckoo = Cuckoo('outD', -720, 1, 0.3, 'cuckoo.wav', 'half_hour.wav')

hourHand = Hand('outB', 'in1', HourFunction, 60.0, 'inversed')
minuteHand = Hand('outA', 'in2', MinuteFunction, 10.0, 'inversed')
#secondHand = Hand('outA', 'in2', SecondFunction, 0.5, 'inversed') # I don't have second hand, but adding one is quite simple
hands = [ hourHand, minuteHand ]

CalibrateHands(hands)

indication.Run()
pendulum.Run()
cuckoo.Run()
for hand in hands:
    hand.Move()

while True:
    sleep(10)
